echo '🐸 💭 You are about to run screaming frog crawl, SEO analysis for headbox.com'

shopt -s expand_aliases
source ~/.bash_profile

ScreamingFrogSEOSpider --crawl "https://www.headbox.com"  --headless --export-tabs "Internal:All" --output-folder "/users/jolantajas/projects/hb/hb-seo-reports/reports" --config "/users/jolantajas/projects/hb/hb-seo-reports/ilp-monthly.seospiderconfig"

echo '🐸 💭 Done ! Report saved in ./reports folder'
