echo '🐸 💭 You are about to run screaming frog crawl, SEO analysis for headbox.com'

shopt -s expand_aliases
source ~/.bash_profile

ScreamingFrogSEOSpider --crawl "https://www.headbox.com" --export-tabs "Internal:All"  --headless --output-folder "/users/jolantajas/projects/hb/hb-seo-reports/reports"  --use-google-analytics "jolanta@headbox.com" "HeadBox" "HeadBox" "All Web Site Data" "All Users" 
# --config "/users/jolantajas/projects/hb/hb-seo-reports/crawl_configuration.seospiderconfig"

echo '🐸 💭 Done ! Report saved in ./reports folder'
