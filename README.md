# Crawl reports for headbox.com landing pages


Uses Screaming frog api and crawl config to generate seo reports - useful posts [here](https://cometfuel.com/learn/automate-screaming-frog/).

Saved reports are to be stored for Pipedout in S3 [`pipedout-screaming-frog-seo-reports`](https://s3.console.aws.amazon.com/s3/home?region=us-east-1#)



## How to generate a report:

run `yarn start`* which runs 'internal_all' monthly report crawling for given url - headbox.com . Alternatively - run any of the individual scripts in the scripts/ folder by `bash ./scripts/<some_script_name>.sh`

*see first time setup bellow



### Upload output example for Pipedout:

- headbox_internal_all_2021_01_01.csv



### First time setup:

1. install latest [Screaming Frog Desktop app](https://www.screamingfrog.co.uk/seo-spider/)
2. Add relevant licensing to use the Screaming frog and other APIs such as GoogleAnalytics, GoogleSearchConsole and other.
3. Create an alias to your .bash_profile for `ScreamingFrogSEOSpider` which points to the location of Screaming Frog launcher. In your `.bash_profile` it should look something like:
`alias ScreamingFrogSEOSpider='/Applications/Screaming\ Frog\ SEO\ Spider.app/Contents/MacOS/ScreamingFrogSEOSpiderLauncher'`



#### Screaming frog command line interface:

provided your .bash_profile is complete with the alias (see First time setup, point 3.) run: `ScreamingFrogSEOSpider -h` to see available commands in order to customise reports

sample of flag use with credential values:

```--use-google-analytics <google account> <account> <property> <view> <segment>```

- google account: <your@email.com>
- account: "HeadBox"
- property: "HeadBox"
- view: "All Web Site Data"
- segment: "All Users"